
# from Img.img import Image_worker
from Img.data import Json_format
from Img.data import Payload


class Process:

    def __init__(self):
        self.tmp = "./tmp/temp.txt"

    def encode_json(self, payload: dict, imgPath: str):
        """ encodes json content form runtime to files """

        # Load to a temp file
        json = Json_format()
        data = json.json_object(payload)
        a = Payload(self.tmp)
        a.make_file(data)

        return self.tmp

    def decode_json(self, imgPath: str):

        a = Payload(self.tmp)
        data = a.read_file()
        return Json_format().json_string(data)

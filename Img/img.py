from steg import steg_img


class Image_worker:

        def encode_image(self, payload: str, imgpath: str):
            obj = steg_img.IMG(
                payload_path=payload,
                image_path=imgpath
            )
            obj.hide()
            return True

        def decode_image(self, imgpath: str):
            obj = steg_img.IMG(
                image_path=imgpath
            )
            obj.extract()
            return True

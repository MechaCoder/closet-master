from json import loads
from json import dumps


class Json_format:

    def json_string(self, raw_content: str):
        """ string to object """

        try:

            if isinstance(raw_content, str) is False:
                raise TypeError("the passed content must be a string")

            text = dumps(raw_content)
            return text

        except TypeError as Err:
            print(Err)
            return Err

    def json_object(self, content: dict):
        """ object to string """
        try:

            if isinstance(content, dict) is False:
                raise TypeError("the passed content must be a dict")

            obj = loads(content)
            return obj

        except TypeError as Err:
            print(Err)
            return Err


class Payload:

    def __init__(self, filepath: str):
        self.path = filepath

    def make_file(self, content: str):

        file = open(self.path, 'w')
        file.write(content)
        file.close()
        return True

    def read_file(self):
        file = open(self.path, 'r')
        data = file.read()
        file.close()

        return data

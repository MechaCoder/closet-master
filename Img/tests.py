import unittest
from json import dumps

from Img.img import Image_worker
from Img.data import Json_format
from Img.data import Payload


class Image_worker_tests(unittest.TestCase):

    def test_encode_image(self):
        worker = Image_worker()
        result = worker.encode_image(
            './temp.txt',
            './randbitmap-rdo.png'
        )

        self.assertEqual(
            isinstance(result, bool),
            True
        )

        self.assertEqual(
            result,
            True
        )

    def test_decode_image(self):
        worker = Image_worker()
        result = worker.decode_image(
            './new.png'
        )

        self.assertEqual(
            isinstance(result, bool),
            True
        )

        self.assertTrue(
            result
        )


class Json_format_tests(unittest.TestCase):

    def json_string_test(self):

        tobj = {"foo": "bar"}
        obj = Json_format()
        data = obj.json_object(tobj)

        self.assertTrue(
            isinstance(data, str)
        )

    def json_object_test(self):

        tobj = {"foo": "bar"}
        data = dumps(tobj)

        testable = Json_format().json_string(data)

        self.assertTrue(
            isinstance(testable, dict)
        )

        self.assertEqual(
            testable,
            tobj
        )


class Payload_tests(unittest.TestCase):

    def make_file_test(self):

        tobj = "test a thing"
        result = Payload('test.txt').make_file(tobj)

        self.assertTrue(
            isinstance(result, bool)
        )

        self.assertEqual(
            result,
            True
        )


if __name__ == '__main__':
    unittest.main()

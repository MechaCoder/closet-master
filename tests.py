import unittest

from locker.tests import Worker_create_password_tests
from locker.tests import Worker_input_tests

from Img.tests import Image_worker_tests
from Img.tests import Json_format_tests
from Img.tests import Payload_tests


if __name__ == '__main__':
    unittest.main()
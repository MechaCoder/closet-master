#! /usr/bin/python3.6
import click
from cli import Cli_support
from cli.files import Files_helper

from Img.img import Image_worker
from locker import File_worker

clis = Cli_support()

@click.group()
def cli():
    pass

@cli.command()
@click.argument('payload')
@click.argument('image')
def encript(payload, image):
    """ encripts a file  """

    # encripted = t.file_input(payload) #the payload temp path
    clis.info("the file has been encripted")
    
    hidden = Image_worker().encode_image(payload, image)
    clis.info("the file has been hidden in your chose images")

    if hidden == True:
        clis.success("the file has be sucsuffly hidden and encripted")        
        print(Files_helper().move_to_output('new.png'))
        clis.info("the file has been placed in the output directory")
        clis.success("Finished")

@cli.command()
@click.argument('image')
def decript(image):
        clis.info('attempting to decript')
        newfile = Image_worker().decode_image(image)
        if newfile == True:
                Files_helper().move_to_output('hidden_file.txt')
                

if __name__ == '__main__':
    cli()
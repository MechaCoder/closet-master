#!/usr/bin/python3.6
from os import path, listdir, rename, mkdir
from time import time

class Files_helper:

    def move_to_output(self, file: str, force_exe: str=''):

        try:
            listdir('output/')
        except FileNotFoundError as err:
            mkdir('output/')

        timestamp = round(time())
        #insureing that the file extention is kept consitent
        fileextention  = file.lower().split('.')[-1]

        path = 'output/{}.{}'.format(timestamp,fileextention)
        if len(force_exe) != 0:
            path = force_exe


        print(path)
        rename(file, path)
        return True

import base64
from string import ascii_letters, digits, punctuation
from random import randint

from Crypto.Cipher import AES


class Passcode_error(BaseException):
    pass


class Worker:

    def __init__(self, key: str=None):
        self.key = key

        if self.key is None:
            self.key = self.create_passcode()

        if len(self.key) < 16:
            self.key = self.create_passcode()
        pass

    def create_passcode(self, length: int=16):

        try:
            if isinstance(length, int) is False:
                raise TypeError("Length has to be an int")

            if length < 16:
                raise Passcode_error(
                    "the passcode is required to be more than 16"
                )

            raw_pool = list(
                ascii_letters
            )

            processed_pool = []
            passcode = []

            while len(raw_pool) != 0:
                randnum = randint(0, len(raw_pool) - 1)
                char = raw_pool[randnum]
                del raw_pool[randnum]

                if randint(0, 1) == 1:
                    processed_pool.append(char)

                else:
                    processed_pool = [char] + processed_pool

            for e in range(0, length):
                num = randint(0, len(processed_pool) - 1)
                passcode.append(processed_pool[num])
            return "".join(passcode)

        except Passcode_error as Error:
            return Error
        except TypeError as Error:
            return Error

    def input(self, msg: str):

        while len(msg) % 16 != 0:
            msg = msg + ""

        codeobj = AES.new(self.key, AES.MODE_ECB)
        encoded = base64.b64encode(codeobj.encrypt(msg))
        return encoded.decode('utf-8')

    def export(self, msg: str):
        print(type(msg))
        codeobj = AES.new(self.key, AES.MODE_ECB)
        decoded = base64.b64decode(msg)
        print(type(decoded))
        able = decoded.strip()
        msgTemp = codeobj.decrypt(able)
        return msgTemp


class File_worker(Worker):

    def file_input(self, filepath: str):

        try:
            file = open(filepath, 'r')
            content = file.read()
            file.close()

            cifer = self.input(content)
            fileid = randint(1,5000)
            file = open('tmp/tmp_{}.encript'.format(fileid), "w")
            file.write(cifer)
            file.close()

            return 'tmp/tmp_{}.encript'.format(fileid)
        except FileNotFoundError:
            print("this file path is invalid")

    def file_export(self, imgpath: str):

        file = open(imgpath, 'r')
        cifer = file.read()
        file.close()

        plane = self.export(cifer)
        fileid = randint(1, 5000)
        filepath = 'tmp/tmp_{}.encript'.format(fileid)
        
        file = open(filepath, 'w')
        file.write(plane)
        file.close()

        return filepath


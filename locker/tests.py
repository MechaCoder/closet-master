import unittest
from random import randint

try:
    from __init__ import Worker, Passcode_error
except ModuleNotFoundError as Error:
    from locker import Worker, Passcode_error


class Worker_create_password_tests(unittest.TestCase):

    def test_init_no_passcode(self):

        obj = Worker()

        self.assertEqual(
            isinstance(obj, Worker),
            True
        )

        self.assertEqual(
            isinstance(obj.key, str),
            True
        )

    def test_init_with_passcode(self):
        passCode = "qwertyqwertyqwer"
        obj = Worker(passCode)

        self.assertEqual(
            obj.key,
            passCode
        )

        obj = Worker("bob")

        self.assertNotEqual(
            obj.key,
            "bob"
        )

    def test_create_passcode(self):

        Worker().create_passcode(4)
        self.assertRaises(Passcode_error)

    def test_create_passcode_2(self):

        Worker().create_passcode("string")
        self.assertRaises(TypeError)

    def test_create_passcode_3(self):

        obj = Worker().create_passcode()

        self.assertTrue(
            isinstance(obj, str)
        )

        self.assertEqual(
            len(obj),
            16
        )

        randnum = randint(0, 1000)

        self.assertEqual(
            len(Worker().create_passcode(randnum)),
            randnum
        )


class Worker_input_tests(unittest.TestCase):

    def setUp(self):
        self.setKey = Worker().create_passcode()

        self.msg_raw = """
            Maybe we got a few little happy bushes here, just covered
            with snow.
            This is the fun part And I know you're saying, 'Oh Bob, you've
            done it this time.' And you may be right. I really believe that if
            you practice enough you could paint the 'Mona Lisa' with a two-inch
            brush. It's hard to see things when you're too close. Take a step
            back and look. There we go. You have to allow the paint to break
            to
            make it beautiful. That's a crooked tree. We'll send him to
            Washington.
            Just let these leaves jump off the brush Working it up and down,
            back and forth. Let's put some highlights on these little trees.
            The sun wouldn't forget them. If you've been in Alaska less than
            a year you're a Cheechako. We don't have anything but happy
            trees here. We must be quiet, soft and gentle. You can do it.
            But they're very easily killed. Clouds are delicate.
            You need the dark in order to show the light. Use what
            you see, don't plan it. The least little bit can do so much.
            A beautiful little sunset. You can work and carry-on and put lots
            of little happy things in here. Just beat the devil out of it.
            You can do anything here. So don't worry about it.
            Maybe there's a happy little Evergreen that lives here.
            Trees get lonely too, so we'll give him a little friend.
            There isn't a rule. You just practice and find out which way
            works best for you. We have all at one time or another mixed
            some mud. Nice little clouds playing around in the sky.
            All you have to learn here is how to have fun.
            If you overwork it you become a cloud killer.
            There's nothing worse than a cloud killer. Let's have a
            little bit of fun today. Zip. That easy.
        """

    def test_input(self):
        obj = Worker(self.setKey)

        x = obj.input(self.msg_raw)

        self.assertTrue(
            isinstance(x, str)
        )

    def text_export(self):
        obj = Worker(self.setKey)
        encripted = obj.input(self.msg_raw)

        decript = obj.export(encripted)

        self.assertTrue(
            isinstance(decript, str)
        )

        self.assertEqual(
            decript,
            self.msg_raw
        )


if __name__ == '__main__':
    unittest.main()
